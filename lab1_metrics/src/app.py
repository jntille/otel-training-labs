import time
from random import choice

import requests
from flask import Flask, request, Response, make_response

from opentelemetry import context
from opentelemetry.propagate import inject, extract

from metric_utils import create_meter, create_http_instruments, create_resource_instruments
from fault_injection import faults, ChaosClient
from opentelemetry.semconv import metrics

app = Flask(__name__)
app.register_blueprint(faults)

@app.before_request
def before_request():
    token = context.attach(extract(request.headers))
    request.environ["context_token"] = token

    workload_instruments["traffic_volume"].add(1)
    request.environ["request_start"] = time.time_ns()

@app.after_request
def after_request(response: Response) -> Response:
    # latency
    workload_instruments["http.server.request.duration"].record(
        amount = (time.time_ns() - request.environ["request_start"]) / 1_000_000_000,
        attributes = {
            "http.request.method": request.method,
            "http.route": request.path,
            "http.response.status_code": response.status_code
        }
    )
    return response

def do_stuff():
    url = "http://httpbin:80/anything"
    try:
        response = requests.get(url)
        return response.text
    except requests.exceptions.ConnectionError as err:
        return

@app.route('/user', methods=['GET'])
def get_user():
    usr, status = client.get_user(123)
    test = {}
    if usr != None:
        test = {
            "id": usr.id,
            "name": usr.name,
            "address": usr.address
        }
    response = make_response(test, status)
    return response

@app.route('/')
def index():
    current_time = time.strftime("%a, %d %b %Y %H:%M:%S", time.gmtime())
    msg = f"Hello, World! It's currently {current_time}"
    workload_instruments['index_counter'].add(1, {"result": choice(['win', 'lose', 'draw']) })
    return msg

if __name__ == "__main__":
    client = ChaosClient()

    # setup metrics
    meter = create_meter("app.py", "0.1")
    workload_instruments = create_http_instruments(meter)
    rc_instruments = create_resource_instruments(meter)
    app.run(host="0.0.0.0", debug = True)