import time
import random

from flask import Blueprint
from faker import Faker

from user import User

fake = Faker()

# GLOBAL
FAULT_BASE_LATENCY_MS = 100
FAULT_TAIL_LATENCY_MS = 3000
FAULT_TAIL_LATENCY_LIKELIHOOD = .1
FAULT_ERROR = 404
FAULT_ERROR_LIKELIHOOD = .1

faults = Blueprint("faults", __name__)


@faults.route('/fault/errorrate', methods=['POST'])
def fault_errorrate():
    pass

@faults.route('/fault/latency', methods=['POST'])
def fault_latency():
    pass


class ChaosClient():
    def get_user(self, id: int) -> (User, int):
        time.sleep(FAULT_BASE_LATENCY_MS/1000)
        if random.random() <= FAULT_TAIL_LATENCY_LIKELIHOOD:
            time.sleep(FAULT_TAIL_LATENCY_MS/1000)
        if random.random() <= FAULT_ERROR_LIKELIHOOD:
            return None, FAULT_ERROR

        usr = User(
            id = random.randint(1, 1000),
            name = fake.name(),
            address = fake.address()
        )
        return usr, 200