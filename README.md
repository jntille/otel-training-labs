# OpenTelemetry Labs

### instructions

```sh
git clone https://gitlab.com/jntille/otel-training-labs.git
cd ./otel-training-labs/lab1_tracing
```

Clone the repository.
Change directory into the lab you want to work on.
Follow the instructions in the README.md