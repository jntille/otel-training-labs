import time
import requests
from flask import Flask, request

app = Flask(__name__)

def do_stuff():
    headers = {}
    url = "http://httpbin:80/anything"
    response = requests.get(url, headers=headers)
    time.sleep(.4)

@app.route('/trace')
def trace():
    time.sleep(.4)
    return "/trace"

@app.route('/')
def index():
    do_stuff()
    current_time = time.strftime("%a, %d %b %Y %H:%M:%S", time.gmtime())
    msg = f"Hello, World! It's currently {current_time}"
    return msg

if __name__ == "__main__":
    app.run(host="0.0.0.0", debug = True)
