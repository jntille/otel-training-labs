# aliases
alias ll='ls -l'
alias la='ls -A'
alias l='ls -CF'

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi


export PS1='\[\e[31m\]\u\[\e[m\]@\h:\w\$ '
